package grofers.flickr

import dagger.Module
import dagger.android.ContributesAndroidInjector
import grofers.flickr.findphotos.FindPhotosFragment

@Module
abstract class AndroidBindingModule {
  @ContributesAndroidInjector
  abstract fun getFindPhotosFragment(): FindPhotosFragment
}
