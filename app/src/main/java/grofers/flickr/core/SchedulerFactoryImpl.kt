package grofers.flickr.core

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.schedulers.Schedulers.io
import javax.inject.Inject

class SchedulerFactoryImpl @Inject constructor() : SchedulerFactory {
    override val mainScheduler: Scheduler = mainThread()
    override val ioScheduler: Scheduler = io()
}
