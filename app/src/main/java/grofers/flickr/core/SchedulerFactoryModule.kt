package grofers.flickr.core

import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class SchedulerFactoryModule {
    @Binds
    @Singleton
    abstract fun schedulerFactory(schedulerFactoryImpl: SchedulerFactoryImpl): SchedulerFactory
}
