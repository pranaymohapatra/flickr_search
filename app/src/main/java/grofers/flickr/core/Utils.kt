package grofers.flickr.core

import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


fun <T> Observable<T>.autoClear(clearRelay: PublishRelay<T>): Observable<T> = this.takeUntil(clearRelay)

fun Disposable.autoDispose(compositeDisposable: CompositeDisposable) = compositeDisposable.add(this)