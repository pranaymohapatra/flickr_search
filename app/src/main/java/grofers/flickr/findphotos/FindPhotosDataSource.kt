package grofers.flickr.findphotos

import android.content.res.Resources
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import grofers.flickr.*
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class FindPhotosDataSource @Inject constructor(private val findPhotosUseCase: FindPhotosUseCase,
                                               private val photoViewModelMapper: PhotoViewModelMapper,
                                               private val resources: Resources)
    : PageKeyedDataSource<Int, PhotoModel>() {

    private lateinit var initialDisposable: Disposable
    private lateinit var nextDisposable: Disposable
    private lateinit var previousDisposable: Disposable

    val isLoading = MutableLiveData<Boolean>()
    val photoCountText = MutableLiveData<String>()
    val isError = MutableLiveData<Boolean>()

    private var mQuery: String = ""

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, PhotoModel>) {
        if (this::initialDisposable.isInitialized && !initialDisposable.isDisposed)
            initialDisposable.dispose()

        initialDisposable = findPhotosUseCase.findPhotos(mQuery)
                .subscribe { result ->
                    when (result) {
                        is Loading -> isLoading.postValue(true)
                        is Success<PhotoResult> -> {
                            val newPhotos = getNewPhotos(result.value.photos)
                            isLoading.postValue(false)
                            updatePhotoCount(result)
                            callback.onResult(newPhotos, null, 2)
                        }
                        is Failure -> {
                            isError.postValue(true)
                            isLoading.postValue(false)
                        }
                    }
                }
    }

    private fun getNewPhotos(result: List<Photo>): List<PhotoModel> {
        return result.map {
            photoViewModelMapper(it)
        }
    }

    private fun updatePhotoCount(result: Success<PhotoResult>) = when {
        result.value.photos.isNotEmpty() -> {
            photoCountText.postValue(resources.getQuantityString(
                    R.plurals.xPhotos,
                    result.value.total,
                    result.value.total
            ))

        }
        else -> photoCountText.postValue("No Photos")
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, PhotoModel>) {
        if (this::nextDisposable.isInitialized && !nextDisposable.isDisposed)
            nextDisposable.dispose()

        nextDisposable = findPhotosUseCase.findPhotos(mQuery)
                .subscribe { result ->
                    when (result) {
                        is Loading -> isLoading.postValue(true)
                        is Success<PhotoResult> -> {
                            val newPhotos = getNewPhotos(result.value.photos)
                            isLoading.postValue(false)
                            updatePhotoCount(result)
                            callback.onResult(newPhotos, params.key + 1)
                        }
                        is Failure -> {
                            isError.postValue(true)
                            isLoading.postValue(false)
                        }
                    }
                }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, PhotoModel>) {
        if (this::previousDisposable.isInitialized && !previousDisposable.isDisposed)
            previousDisposable.dispose()

        previousDisposable = findPhotosUseCase.findPhotos(mQuery)
                .subscribe { result ->
                    when (result) {
                        is Loading -> isLoading.postValue(true)
                        is Success<PhotoResult> -> {
                            val newPhotos = getNewPhotos(result.value.photos)
                            isLoading.postValue(false)
                            updatePhotoCount(result)

                            val prev = if (params.key - 1 < 0)
                                0
                            else
                                params.key - 1

                            callback.onResult(newPhotos, prev)
                        }
                        is Failure -> {
                            isError.postValue(true)
                            isLoading.postValue(false)
                        }
                    }
                }
    }

    fun setQuery(query: String) {
        mQuery = query
    }

    fun disposeAll() {

    }
}