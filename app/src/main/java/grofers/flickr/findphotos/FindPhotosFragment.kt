package grofers.flickr.findphotos

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import grofers.flickr.R
import grofers.flickr.core.DaggerFragment
import grofers.flickr.databinding.PhotosFragmentBinding
import javax.inject.Inject

class FindPhotosFragment : DaggerFragment() {
    @Inject
    lateinit var viewModel: FindPhotosViewModel

    private lateinit var photosListAdapter: PhotosListAdapter
    private lateinit var photosView: RecyclerView

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true

        viewModel.loadPhotos(savedInstanceState)
        viewModel.onErrorLoadingPhotos.observe(this, Observer {
            view?.let {
                Snackbar
                        .make(it, R.string.cannot_load_photos, Snackbar.LENGTH_LONG)
                        .setAction(R.string.retry) { viewModel.retry() }
                        .show()
            }
        })

        viewModel.rebindLiveData.observe(this, Observer {
            rebind()
        })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        viewModel.onSaveInstanceState(outState)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val binding = PhotosFragmentBinding.inflate(inflater)

        binding.searchView.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                viewModel.onSearchTextSubmit(s.toString())
            }

        })

        photosView = binding.photosView.also {
            it.layoutManager = GridLayoutManager(context, 2)
        }
        binding.mViewModel = viewModel
        addDividers(binding.photosView)

        rebind()

        return binding.root
    }

    private fun rebind() {
        context?.let { photosListAdapter = PhotosListAdapter(it) }
        if (this::photosListAdapter.isInitialized)
            photosView.adapter = photosListAdapter
        viewModel.pagedPhotos.observe(this, Observer {
            photosListAdapter.submitList(it)
        })
    }

    private fun addDividers(photosView: RecyclerView) {
        val verticalDividerItemDecoration = DividerItemDecoration(
                activity, DividerItemDecoration.VERTICAL
        ).apply {
            setDrawable(getDrawable(requireContext(), R.drawable.vertical_divider)!!)
        }
        photosView.addItemDecoration(verticalDividerItemDecoration)

        val horizontalDividerItemDecoration = DividerItemDecoration(
                activity, DividerItemDecoration.HORIZONTAL
        ).apply {
            setDrawable(getDrawable(requireContext(), R.drawable.horizontal_divider)!!)
        }
        photosView.addItemDecoration(horizontalDividerItemDecoration)
    }
}
