package grofers.flickr.findphotos

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import grofers.flickr.FindPhotosUseCase
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject


internal const val KEY_QUERY = "query"

@SuppressLint("CheckResult")
class FindPhotosViewModel @Inject internal constructor(
        private var findPhotosUseCase: FindPhotosUseCase,
        private val photoSourceFactory: PhotoSourceFactory
) : ViewModel() {


    var rebindLiveData = MutableLiveData<Boolean>()
    private val dataSourceObserver: Observer<in FindPhotosDataSource?> = Observer {
        it?.let {
            findPhotosDataSource = it
            findPhotosDataSource.run {
                isError.observeForever(errorObserver)
                isLoading.observeForever(loadingObserver)
                photoCountText.observeForever(photoCountObserver)
            }
        }
    }

    private val loadingObserver: Observer<in Boolean?> = Observer {
        it?.let {
            isLoading.set(it)

        }
    }


    private val photoCountObserver: Observer<in String?> = Observer {
        it?.let {
            photoCountText.set(it)
        }
    }

    private val errorObserver: Observer<in Boolean?> = Observer {
        it?.let {
            onErrorLoadingPhotos.value = true

        }
    }

    private lateinit var findPhotosDataSource: FindPhotosDataSource
    val title = ObservableField<String>()
    val isLoading = ObservableField<Boolean>()
    val photoCountText = ObservableField<String>()
    val onErrorLoadingPhotos = MutableLiveData<Boolean>()
    lateinit var pagedPhotos: LiveData<PagedList<PhotoModel?>?>


    private lateinit var queryTextEmitter: ObservableEmitter<String>

    private val compositeDisposable = CompositeDisposable()


    init {
        compositeDisposable.add(Observable.create { emitter: ObservableEmitter<String> ->
            queryTextEmitter = emitter
        }.debounce(500, TimeUnit.MILLISECONDS).subscribeOn(Schedulers.io())
                .subscribe {
                    findPhotos(it)
                }
        )
        findPhotos("cats")
        photoSourceFactory.getMutableLiveData().observeForever(dataSourceObserver)
    }


    fun loadPhotos(savedInstanceState: Bundle?) {
        val query = savedInstanceState?.getString(KEY_QUERY)
        query?.let { findPhotos(it) }
    }

    fun retry() {
        findPhotosUseCase.retry()
    }

    fun onSaveInstanceState(outState: Bundle?) {
        outState?.putString(KEY_QUERY, findPhotosUseCase.lastQuery)
    }


    fun onSearchTextSubmit(query: String) {
        queryTextEmitter.onNext(query)
    }

    private fun findPhotos(query: String) {

        val pagedListConfig = PagedList.Config.Builder()
                .setEnablePlaceholders(true)
                .setInitialLoadSizeHint(20)
                .setPageSize(20).build()

        title.set(query)

        photoSourceFactory.setQuery(query)
        pagedPhotos = LivePagedListBuilder(photoSourceFactory, pagedListConfig)
                .build()
        rebindLiveData.postValue(true)
    }

    override fun onCleared() {
        super.onCleared()
        photoSourceFactory.getMutableLiveData().removeObserver(dataSourceObserver)
    }
}
