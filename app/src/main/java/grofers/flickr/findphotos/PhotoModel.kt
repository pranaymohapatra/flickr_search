package grofers.flickr.findphotos

import android.net.Uri
import androidx.annotation.NonNull
import androidx.recyclerview.widget.DiffUtil
import java.util.*


typealias PhotoId = String

data class PhotoModel(
        val id: PhotoId = UUID.randomUUID().toString(),
        val link: Uri,
        val title: String? = null
)

var Item_Callback: DiffUtil.ItemCallback<PhotoModel?> = object : DiffUtil.ItemCallback<PhotoModel?>() {
    override fun areItemsTheSame(@NonNull oldItem: PhotoModel, @NonNull newItem: PhotoModel): Boolean {
        return oldItem.id === newItem.id
    }

    override fun areContentsTheSame(@NonNull oldItem: PhotoModel, @NonNull newItem: PhotoModel): Boolean {
        return oldItem == newItem
    }
}


