package grofers.flickr.findphotos

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import javax.inject.Inject


class PhotoSourceFactory @Inject constructor(private val photosDataSource: FindPhotosDataSource)
    : DataSource.Factory<Int, PhotoModel>() {

    private val mutableLiveData: MutableLiveData<FindPhotosDataSource?> = MutableLiveData()

    private var mQuery: String = ""

    override fun create(): DataSource<Int, PhotoModel> {
        photosDataSource.setQuery(mQuery)
        mutableLiveData.postValue(photosDataSource)
        return photosDataSource
    }

    fun getMutableLiveData(): MutableLiveData<FindPhotosDataSource?> {
        return mutableLiveData
    }

    fun setQuery(query: String) {
        mQuery = query
    }

}