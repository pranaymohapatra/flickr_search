package grofers.flickr.findphotos

import android.net.Uri
import grofers.flickr.Photo
import javax.inject.Inject

open class PhotoViewModelMapper @Inject internal constructor() {
  open operator fun invoke(photo: Photo) = PhotoModel(
      id = photo.id,
      link = Uri.parse(photo.link),
      title = photo.title
  )
}
