package grofers.flickr.findphotos

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView

import grofers.flickr.databinding.NetworkItemBinding
import grofers.flickr.databinding.PhotoItemBinding

class PhotosListAdapter(private val context: Context) :
        PagedListAdapter<PhotoModel?, RecyclerView.ViewHolder>(Item_Callback) {

    private var networkState: NetworkState? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return if (viewType == TYPE_PROGRESS) {
            val headerBinding: NetworkItemBinding = NetworkItemBinding.inflate(layoutInflater, parent, false)
            NetworkStateItemViewHolder(headerBinding)
        } else {
            val itemBinding: PhotoItemBinding = PhotoItemBinding.inflate(layoutInflater, parent, false)
            PhotoItemViewHolder(itemBinding)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is PhotoItemViewHolder) {
            holder.bindTo(getItem(position))
        } else {
            (holder as NetworkStateItemViewHolder).bindView(networkState)
        }
    }

    private fun hasExtraRow(): Boolean {
        return networkState != null && networkState !== NetworkState.LOADED
    }

    override fun getItemViewType(position: Int): Int {
        return if (hasExtraRow() && position == itemCount - 1) {
            TYPE_PROGRESS
        } else {
            TYPE_ITEM
        }
    }

    fun setNetworkState(newNetworkState: NetworkState) {
        val previousState: NetworkState? = networkState
        val previousExtraRow = hasExtraRow()
        networkState = newNetworkState
        val newExtraRow = hasExtraRow()
        if (previousExtraRow != newExtraRow) {
            if (previousExtraRow) {
                notifyItemRemoved(itemCount)
            } else {
                notifyItemInserted(itemCount)
            }
        } else if (newExtraRow && previousState !== newNetworkState) {
            notifyItemChanged(itemCount - 1)
        }
    }

    inner class PhotoItemViewHolder(private val binding: PhotoItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindTo(photoModel: PhotoModel?) {
            binding.myImageView.setImageURI(photoModel?.link)
            binding.photoTitle.text = photoModel?.title
        }

    }

    inner class NetworkStateItemViewHolder(private val binding: NetworkItemBinding) : RecyclerView.ViewHolder(binding.getRoot()) {
        fun bindView(networkState: NetworkState?) {
            if (networkState != null && networkState.status === NetworkState.Status.RUNNING) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
            if (networkState != null && networkState.status === NetworkState.Status.FAILED) {
                binding.errorMsg.visibility = View.VISIBLE
                binding.errorMsg.setText(networkState.msg)
            } else {
                binding.errorMsg.visibility = View.GONE
            }
        }

    }

    companion object {
        private const val TYPE_PROGRESS = 0
        private const val TYPE_ITEM = 1
    }

}