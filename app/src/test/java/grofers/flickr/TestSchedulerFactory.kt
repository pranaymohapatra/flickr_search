package grofers.flickr

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import grofers.flickr.core.SchedulerFactory

class TestSchedulerFactory : SchedulerFactory {
  override val mainScheduler: Scheduler
    get() = Schedulers.trampoline()

  override val ioScheduler: Scheduler
    get() = Schedulers.trampoline()
}
