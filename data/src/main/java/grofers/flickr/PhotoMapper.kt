package grofers.flickr

internal open class PhotoMapper {
    open operator fun invoke(response: PhotosResponseEntity): PhotoResult {
        val entities = response.photos()?.photos() ?: emptyList<PhotoEntity>()

        val photos = entities.filter { it.thumbnailUrl() != null }
                .map { toPhoto(it) }


        return PhotoResult(photos, response.photos()?.total() ?: 0)
    }

    open fun toPhoto(entity: PhotoEntity): Photo = Photo(
            id = entity.id(),
            link = checkNotNull(entity.thumbnailUrl()) {
                "Thumbnail url doesn't exist"
            },
            title = entity.title()
    )


    private fun PhotoEntity.thumbnailUrl() = with(this) {
        url_t() ?: url_s() ?: url_q() ?: url_m() ?: url_n()
        ?: url_z() ?: url_c() ?: url_l() ?: url_o() ?: url_sq()
    }
}
