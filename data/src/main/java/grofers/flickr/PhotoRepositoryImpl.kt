package grofers.flickr

import android.util.Log
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

internal class PhotoRepositoryImpl @Inject internal constructor(
        private val api: FlickrApi,
        private val photoMapper: PhotoMapper
) : PhotoRepository {
    override fun search(query: String, page: Int): Flowable<Result<PhotoResult>> =
            api.search(API_KEY, query, page).toPhotos()

    private fun Single<PhotosResponseEntity>.toPhotos(): Flowable<Result<PhotoResult>> {
        return this.toFlowable()
                .map { photoMapper(it) }
                .map { it }
                .map<Result<PhotoResult>> {
                    Success(it)
                }
                .doOnError {
                    Log.e(this.javaClass.name, it.localizedMessage ?: "Error getting photos")
                }
                .onErrorReturn { Failure(it) }
                .startWith(Loading())
    }
}
