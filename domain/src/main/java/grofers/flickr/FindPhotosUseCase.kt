package grofers.flickr

import io.reactivex.Flowable

interface FindPhotosUseCase {
    var lastQuery: String
    fun findPhotos(query: String) : Flowable<Result<PhotoResult>>
    fun getPage(page : Int) : Flowable<Result<PhotoResult>>
    fun retry() : Flowable<Result<PhotoResult>>
}