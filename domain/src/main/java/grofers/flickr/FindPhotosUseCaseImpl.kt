package grofers.flickr

import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class FindPhotosUseCaseImpl @Inject constructor(private val photoRepository: PhotoRepository)
    : FindPhotosUseCase {

    override var lastQuery: String = ""

    override fun findPhotos(query: String): Flowable<Result<PhotoResult>> {
        lastQuery = query
        return photoRepository.search(query, 1).subscribeOn(Schedulers.io())
    }

    override fun getPage(page: Int): Flowable<Result<PhotoResult>> {
        return photoRepository.search(lastQuery, page).subscribeOn(Schedulers.io())
    }

    override fun retry(): Flowable<Result<PhotoResult>> {
        return findPhotos(lastQuery)
    }
}