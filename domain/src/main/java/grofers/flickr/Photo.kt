package grofers.flickr

import java.util.*

data class Photo(
        val id: String = UUID.randomUUID().toString(),
        val link: String,
        val title: String? = null
)

data class PhotoResult(
        val photos: List<Photo>, val total: Int
)
