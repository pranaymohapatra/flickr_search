package grofers.flickr

import io.reactivex.Flowable
import io.reactivex.Single



interface PhotoRepository {
  /**
   * Returns a list of the latest public photos uploaded to flickr.
   */

  fun search(query: String,page:Int): Flowable<Result<PhotoResult>>
}
