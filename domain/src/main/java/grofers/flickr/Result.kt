package grofers.flickr

sealed class Result<T>

class Loading<T> : Result<T>() {
  override fun equals(other: Any?): Boolean = other is Loading<*>
  override fun hashCode(): Int {
    return javaClass.hashCode()
  }
}

data class Success<T>(val value: T) : Result<T>()

data class Failure<T>(val error: Throwable) : Result<T>()
