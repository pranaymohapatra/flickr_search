package grofers.flickr

import dagger.Binds

import dagger.Module

@Module
abstract class UseCaseModule{
    @Binds
    abstract fun getFindPhotosUseCase(findPhotosUseCaseImpl: FindPhotosUseCaseImpl) : FindPhotosUseCase
}